#include <stdio.h>
#include <stdlib.h>
#include "logic.h"

int readInput(const char* fileName) {

    FILE* file = fopen(fileName, "r");

    fscanf(file, "%d %d %d", &rows, &cols, &steps);
    if (rows > 10 || rows < 2)
    {
        printf("Invalid value for rows.\n");
        return 0;
    }
    if (cols > 10 || cols < 2)
    {
        printf("Invalid value for colums.\n");
        return 0;
    }
    if (steps > 20 || steps < 1)
    {
        printf("Invalid value for steps.\n");
        return 0;
    }
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            fscanf(file, "%d,", &grid[i][j]);
        }
    }

    fclose(file);
    return 1;
}

int writeOutput(const char* fileName) {
    FILE* file = fopen(fileName, "w");
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            fprintf(file, "%d,", grid[i][j]);
        }
        fprintf(file, "\n");
    }
return 1;
}
