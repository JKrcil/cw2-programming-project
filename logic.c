#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
#include "SDL.h"

int nextGen[10][10];

void displayGrid()
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }
}
int nextGeneration()
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            nextGen[i][j] = 0;
        }
    }

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            int aliveNeighbors = 0;
            for (int k = -1; k <= 1; k++)
            {
                for (int l = -1; l <= 1; l++)
                {
                    aliveNeighbors += grid[i + k][j + l];
                }
            }
            aliveNeighbors -= grid[i][j];
            if (grid[i][j] == 1 && (aliveNeighbors < 2))
            {
                nextGen[i][j] = 0;
            }
            else if (grid[i][j] == 1 && (aliveNeighbors > 3)) {
                nextGen[i][j] = 0;
            }
            else if (grid[i][j] == 0 && (aliveNeighbors == 3)) {
                nextGen[i][j] = 1;
            }
            else
                nextGen[i][j] = grid[i][j];
        }
    }
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            grid[i][j] = nextGen[i][j];
        }

    }
    return 1;
}
