
#include <SDL2/SDL.h>
#include <stdio.h>
#include "graphics.h"
#include "logic.h"

void drawGrid(SDL_Renderer* render){ //draws the grid using SDL
  for (int i = 0; i < rows; i++) {
      for (int j = 0; j < cols; j++) {
          SDL_Rect rect;
          rect.x = j * 51;
          rect.y = i * 51;
          rect.w = 50;
          rect.h = 50;
          if (grid[i][j] == 1) {
              SDL_SetRenderDrawColor(render, 159, 84, 8, 255);
              SDL_RenderFillRect(render, &rect);
          }
          else
          {
              SDL_SetRenderDrawColor(render, 105, 105, 105, 105);
              SDL_RenderFillRect(render, &rect);
          }
      }
  }
}
int graphics()
{
    int quit = 1;


    SDL_Event event;

    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_Window* window = SDL_CreateWindow("Game of life", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, 680, 680, 0);

    SDL_Renderer* render = SDL_CreateRenderer(window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

        drawGrid(render);

        int step = 0;
        while (quit == 1 ||  step !=steps) {
            SDL_WaitEvent(&event);
            SDL_RenderPresent(render);

            if (event.type == SDL_QUIT) {
                quit = 0;
                break;
            }
            else if (event.type == SDL_KEYDOWN && step<steps)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_1:
                    step++;
                    nextGeneration();
                    drawGrid(render);
                    break;
                }
            }
        }
    SDL_Quit();
    return 0;
}
