
#include <SDL.h>
#include <stdio.h>

SDL_Rect newSDL_Rect(int xs, int ys, int widths, int heights)
{
    SDL_Rect rectangular;
    rectangular.x = xs;
    rectangular.y = ys;
    rectangular.w = widths;
    rectangular.h = heights;
    return rectangular;
}
int rows = 5;
int cols = 5;
int grid[5][5] = { {0,0,0,0,0},{0,0,1,0,0},{1,0,1,0,0},{0,1,1,0,0},{0,0,0,0,0} };
int nextGen[5][5];
void nextGeneration()
{
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            nextGen[i][j] = 0;
        }
    }

    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            int aliveNeighbors = 0;
            for (int k = -1; k <= 1; k++)
            {
                for (int l = -1; l <= 1; l++)
                {
                    aliveNeighbors += grid[i + k][j + l];
                }
            }
            aliveNeighbors -= grid[i][j];
            if (grid[i][j] == 1 && (aliveNeighbors < 2))
            {
                nextGen[i][j] = 0;
            }
            else if (grid[i][j] == 1 && (aliveNeighbors > 3)) {
                nextGen[i][j] = 0;
            }
            else if (grid[i][j] == 0 && (aliveNeighbors == 3)) {
                nextGen[i][j] = 1;
            }
            else
                nextGen[i][j] = grid[i][j];
        }
    }
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            grid[i][j] = nextGen[i][j];
        }

    }
}
int main()
{
    int quit = 1;

 
    SDL_Event event;

    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_Window* window = SDL_CreateWindow("Game of life", SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED, 680, 680, 0);

    SDL_Renderer* render = SDL_CreateRenderer(window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    int startPos = 0;
    for (int i = 0; i < 4; i++) {
   
        for (int y = 0; y < 5; y++) {
            for (int x = startPos; x < 5; x++) {
                SDL_Rect rect;
                rect.x = x * 51;
                rect.y = y * 51;
                rect.w = 50;
                rect.h = 50;
                if (grid[y][x] == 1) {
                    SDL_SetRenderDrawColor(render, 159, 84, 8, 255);
                    SDL_RenderFillRect(render, &rect);
                }
                else
                {
                    SDL_SetRenderDrawColor(render, 105, 105, 105, 105);
                    SDL_RenderFillRect(render, &rect);
                }

            }
            

        }
        SDL_RenderPresent(render);
        while (quit == 1) {
            SDL_WaitEvent(&event);

            switch (event.type) {
            case SDL_QUIT:
                quit = 0;
                break;
            case SDL_KEYDOWN:
            }
        }
    }
    
    SDL_Quit();
    return 0;
}