#include "unity.h"
#include "logic.h"
#include "IO.h"
void test_readInput() {

	int output = readInput("test1.txt");//rows = 11
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, output, "Checking for correct output");


	output = readInput("test2.txt");//rows = 10
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");
	TEST_ASSERT_MESSAGE(rows == 10, "Checking correct value of rows");

	output = readInput("test3.txt");//rows = 2
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");


	output = readInput("test4.txt");//rows = 1
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, output, "Checking for correct output");


	output = readInput("test5.txt");//rows = 5
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");


	output = readInput("test6.txt");//colums = 11
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, output, "Checking for correct output");

	output = readInput("test7.txt");//colums = 10
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");
	TEST_ASSERT_MESSAGE(cols == 10, "Checking correct value of colums");


	output = readInput("test8.txt");//colums = 2
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");

	output = readInput("test9.txt");//colums = 1
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, output, "Checking for correct output");

	output = readInput("test10.txt");//steps = 20
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");
	TEST_ASSERT_MESSAGE(steps == 20, "Checking correct value of steps");

	output = readInput("test11.txt");//steps = 21
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, output, "Checking for correct output");

	output = readInput("test12.txt");//steps = 1
	TEST_ASSERT_EQUAL_INT_MESSAGE(1, output, "Checking for correct output");

	output = readInput("test13.txt");//steps = 0
	TEST_ASSERT_EQUAL_INT_MESSAGE(0, output, "Checking for correct output");

}
void setUp(){

}
void tearDown(){

}
int main() {
	UNITY_BEGIN();
	RUN_TEST(test_readInput);
  return UNITY_END();
}
