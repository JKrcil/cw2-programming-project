#include <stdio.h>
#include <stdlib.h>

int readInput(const char* fileName); //reads the size of the grid, number of steps and the world from a file. Returns 1 on success, 0 on failure to read the data
int writeOutput(const char* fileName);
