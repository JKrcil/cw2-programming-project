#include <stdio.h>
#include <stdlib.h>
#include "logic.h"
#include "graphics.h"
#include "IO.h"



int main()
{
  if (readInput("input.txt") == 0) {
    return 0;
  }

    graphics();
    writeOutput("output.txt");
}
