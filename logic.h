#include <stdio.h>
#include <stdlib.h>

#define MAX_GRID = 10; //maximum size of the grid

int grid[10][10];

int rows;//number of rows in the grid
int cols; //number of colums in the grid
int steps; //maximum nuber of steps the user can take
void displayGrid(); //displays the grid in the cmd
int nextGeneration(); // method which encapsulates the rules of the Game Of Life. Changes the current grid array into the next generation on method call
